import React, { useEffect } from 'react';
import onWindowResize from './window-resize';
const DynamicViewportHeight = () => {
  useEffect(() => {
    onWindowResize();
    window.addEventListener('resize', onWindowResize);
    return () => window.removeEventListener('resize', onWindowResize);
  }, []);
  return /*#__PURE__*/React.createElement(React.Fragment, null);
};
export default DynamicViewportHeight;
const onWindowResize = () => {
    const root = document.querySelector(':root');
    root.style.setProperty('--dvh', `${window.innerHeight}px`);
};
export default onWindowResize;

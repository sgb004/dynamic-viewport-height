# Dynamic viewport height

[![Version](https://img.shields.io/badge/version-1.0.4-blue.svg)](https://www.npmjs.com/package/sgb004-svg-ani-lines)
[![Author](https://img.shields.io/badge/author-sgb004-green.svg)](https://sgb004.com)

This module tries to give compatibility to some browsers that does not support dynamic units of the viewport, specifically the height (dvh).

This module may be unnecessary in the next years, so I recommend you check [https://caniuse.com/?search=dvh](https://caniuse.com/?search=dvh) to know if it is necessary to use it in your project.

<!-- Examples -->

## Installation

To install **dynamic viewport height** you can use npm:

```node
npm i sgb004-dynamic-viewport-height
```

## Usage

### Web

You can download `dynamic-viewport-height.js` from the dist directory and add them to your HTML.

### React

In React you can use the component `DynamicViewportHeight`:

```jsx
import DynamicViewportHeight from 'sgb004-dynamic-viewport-height';

const MyComponent = () => (
	<>
		<DynamicViewportHeight />
		{/* My content */}
	</>
);

export default MyComponent;
```

## Use in CSS

Once you add the code or component, you can use it in CSS in the following way:

```css
.my-box {
	height: var(--dvh, 100dvh);
}

.my-other-box {
	height: calc(var(--dvh, 100dvh) / 2); /* = 50dvh */
}
```

I suggest adding `100dvh` so that in the future you can remove the component or code from this module and the CSS will continue working.

## Author

[sgb004](https://sgb004.com)

## License

MIT

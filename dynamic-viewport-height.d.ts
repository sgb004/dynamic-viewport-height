import React from 'react';
declare const DynamicViewportHeight: () => React.JSX.Element;
export default DynamicViewportHeight;

declare const onWindowResize: () => void;
export { onWindowResize };

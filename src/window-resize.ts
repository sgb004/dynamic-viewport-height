const onWindowResize = () => {
	const root = document.querySelector(':root') as HTMLElement;
	root.style.setProperty('--dvh', `${window.innerHeight}px`);
};

export default onWindowResize;

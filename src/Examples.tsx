import React, { useEffect, useState } from 'react';
import DynamicViewportHeight from './DynamicViewportHeight';

const Examples = () => {
	let [dvh, setDvh] = useState('');

	useEffect(() => {
		const onResize = () => {
			const root = document.querySelector(':root') as HTMLElement;
			const dvh = root.style.getPropertyValue('--dvh');
			setDvh(dvh);
		};
		onResize();
		window.addEventListener('resize', onResize);

		return () => window.removeEventListener('resize', onResize);
	}, []);

	return (
		<div id="examples">
			<h2>Example</h2>
			<DynamicViewportHeight /> <div id="box-dvh">Window height: {dvh}</div>
		</div>
	);
};

export default Examples;

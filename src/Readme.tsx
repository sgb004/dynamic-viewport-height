import Examples from './Examples';

const Readme = () => (
	<div id="readme">
	<h1>Dynamic viewport height</h1>
<p><a href="https://www.npmjs.com/package/sgb004-svg-ani-lines"><img src="https://img.shields.io/badge/version-1.0.4-blue.svg" alt="Version" /></a>
<a href="https://sgb004.com"><img src="https://img.shields.io/badge/author-sgb004-green.svg" alt="Author" /></a></p>
<p>This module tries to give compatibility to some browsers that does not support dynamic units of the viewport, specifically the height (dvh).</p>
<p>This module may be unnecessary in the next years, so I recommend you check <a href="https://caniuse.com/?search=dvh">https://caniuse.com/?search=dvh</a> to know if it is necessary to use it in your project.</p>
<Examples />
<h2>Installation</h2>
<p>To install <strong>dynamic viewport height</strong> you can use npm:</p>
<pre><code>npm i sgb004-dynamic-viewport-height<br/></code></pre>
<h2>Usage</h2>
<h3>Web</h3>
<p>You can download <code>dynamic-viewport-height.js</code> from the dist directory and add them to your HTML.</p>
<h3>React</h3>
<p>In React you can use the component <code>DynamicViewportHeight</code>:</p>
<pre><code>import DynamicViewportHeight from 'sgb004-dynamic-viewport-height';<br/><br/>const MyComponent = () =&gt; (<br/>    &lt;&gt;<br/>        &lt;DynamicViewportHeight /&gt;<br/>        &#123;/* My content */&#125;<br/>    &lt;/&gt;<br/>);<br/><br/>export default MyComponent;<br/></code></pre>
<h2>Use in CSS</h2>
<p>Once you add the code or component, you can use it in CSS in the following way:</p>
<pre><code>.my-box &#123;<br/>    height: var(--dvh, 100dvh);<br/>&#125;<br/><br/>.my-other-box &#123;<br/>    height: calc(var(--dvh, 100dvh) / 2); /* = 50dvh */<br/>&#125;<br/></code></pre>
<p>I suggest adding <code>100dvh</code> so that in the future you can remove the component or code from this module and the CSS will continue working.</p>
<h2>Author</h2>
<p><a href="https://sgb004.com">sgb004</a></p>
<h2>License</h2>
<p>MIT</p>
	</div>
);

export default Readme;
	